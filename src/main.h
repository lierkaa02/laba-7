#define ACCURACY 0.001 ///< З ітераційної формули Герона константа лише одна - це точність розрахування кореня квадратного
///Створюємо константу, що буде визначати кількість рядків та стовпчиков у матриці
#define SIZE 3

///Робимо попередню декларацію для функцій, використованих у цій программі
double module(double number);
double sqr(double number);
double sqrt_from_number(double given_number);
int multiple_matrix(int matrix_string[SIZE * SIZE]);
///Функції для виводу окремих програм
int cycles(void);
int arrays(void);
